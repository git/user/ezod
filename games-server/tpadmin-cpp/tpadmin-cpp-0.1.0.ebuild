# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit games

DESCRIPTION="A command-line administration utility for Thousand Parsec servers."
HOMEPAGE="http://www.thousandparsec.net/tp/"
SRC_URI="
	${HOMEPAGE}/downloads/${PN}/${P}.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	dev-libs/boost
	>=dev-libs/libtprl-0.1.2
	>=dev-games/libtpproto-cpp-0.1.9
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${P}-stdint.patch"
)

DOCS="AUTHORS ChangeLog NEWS README"

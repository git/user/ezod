# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
PYTHON_DEPEND="2:2.6"

inherit distutils eutils

DESCRIPTION="FuzzPy is a library for fuzzy sets, fuzzy graphs, and general fuzzy
mathematics for Python."
HOMEPAGE="http://github.com/ezod/fuzzpy"
SRC_URI="mirror://pypi/f/${PN}/${P}.tar.bz2"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc gnuplot pydot"

RDEPEND="
	gnuplot? ( dev-python/gnuplot-py )
	pydot? ( media-gfx/pydot )
"
DEPEND="${RDEPEND}
	dev-python/setuptools
	doc? ( dev-python/epydoc )
"

DOCS="AUTHORS RELEASE-NOTES"

src_install() {
	distutils_src_install
	if use doc; then
		env python setup.py doc
		dohtml -r doc/
	fi
}

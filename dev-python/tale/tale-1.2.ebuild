# Distributed under the terms of the GNU General Public License v2

EAPI="4-python"
PYTHON_DEPEND="<<[tk?]>>"
PYTHON_MULTIPLE_ABIS=1

inherit distutils

DESCRIPTION="Tale is a framework for creating interactive fiction (text adventures), or MUDs (multi-user dungeons)."
HOMEPAGE="http://pythonhosted.org/tale"
SRC_URI="mirror://pypi/t/${PN}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="tk"

DEPEND=""
RDEPEND="${DEPEND}"

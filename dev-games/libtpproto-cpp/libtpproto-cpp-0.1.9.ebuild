# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit base

DESCRIPTION="A C++ protocol library for Thousand Parsec game clients"
HOMEPAGE="http://www.thousandparsec.net/tp/"
SRC_URI="
	${HOMEPAGE}/downloads/${PN}/${P}.tar.gz
"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc gnutls"

RDEPEND="
	dev-libs/boost
	gnutls? ( >=net-libs/gnutls-1.2.10 )
"
DEPEND="${RDEPEND}
	doc? ( app-doc/doxygen )
"

PATCHES=(
	"${FILESDIR}/${P}-stdint.patch"
)

DOCS="AUTHORS ChangeLog README"

src_configure() {
	econf \
		$(use_enable gnutls) \
		|| die "econf failed"
}

src_compile() {
	base_src_compile

	if use doc; then
		make doc || ewarn "make doc failed".
	fi
}

src_install() {
	base_src_install

	if use doc; then
		insinto /usr/share/doc/${PF}
		dohtml -r docs/html/*
	fi
}

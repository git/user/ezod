# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
PYTHON_DEPEND="2:2.6"

inherit distutils

MY_P=SimpleCV-${PV}

DESCRIPTION="Python interface to several powerful open source computer vision libraries."
HOMEPAGE="http://simplecv.org/"
SRC_URI="mirror://sourceforge/${PN}/${PV}/${MY_P}.tar.gz"

LICENSE="Ingenuitas"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ocr test"

DEPEND="
	test? ( net-misc/wget dev-python/nose )
"
RDEPEND="
	>=media-libs/opencv-2.2
	dev-python/numpy
	sci-libs/scipy
	dev-python/imaging
	dev-python/pygame
	dev-python/ipython
	ocr? ( app-text/tesseract app-text/python-tesseract )
"

S=${WORKDIR}/${MY_P}

src_test() {
	mkdir "${S}/SimpleCV/sampleimages"
	cd "${S}/SimpleCV/sampleimages"
	for im in \
		9dots4lines.png \
		CalibImage0.png \
		CalibImage1.png \
		CalibImage2.png \
		CalibImage3.png \
		CalibImage4.png \
		CalibImage5.png \
		CalibImage6.png \
		CalibImage7.png \
		CalibImage8.png \
		CalibImage9.png \
		aerospace.jpg \
		barcode.png \
		black.png \
		blockhead.png \
		cam.jpg \
		chull.png \
		greenscreen.png \
		greyscale.jpg \
		hardblob.png \
		icecave.png \
		justapixel.png \
		logo-trans.png \
		logo.png \
		logo_inverted.png \
		mtest.png \
		ocr-test.png \
		orson_welles.jpg \
		pills.png \
		simplecv.png \
		statue_liberty.jpg \
		template.png \
		templatetest.png \
		test.png \
		troll_face.png \
		white.png; do
		wget "https://github.com/ingenuitas/SimpleCV/raw/9e34e9552d287b3f308360cc152fea3398e93604/SimpleCV/sampleimages/$im"
	done
	cd "${S}/SimpleCV/tests"
	nosetests tests.py || die "tests failed"
}

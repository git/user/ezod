# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="A C++ library for GNU Readline"
HOMEPAGE="http://www.thousandparsec.net/tp/"
SRC_URI="${HOMEPAGE}/downloads/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	>=sys-libs/readline-5.2_p1
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${P}-readline.patch"
)

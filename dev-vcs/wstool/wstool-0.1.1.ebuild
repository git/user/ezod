# Distributed under the terms of the GNU General Public License v2

EAPI="4-python"
PYTHON_MULTIPLE_ABIS="1"

inherit distutils

DESCRIPTION="wstool provides commands to manage several local SCM repositories."
HOMEPAGE="http://www.ros.org"
SRC_URI="mirror://pypi/w/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="dev-python/setuptools
		dev-python/vcstools"
RDEPEND="${DEPEND}"


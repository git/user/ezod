# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils java-pkg-2

DESCRIPTION="Swarm is a platform for agent-based models."
HOMEPAGE="http://www.swarm.org"
SRC_URI="http://pj.freefaculty.org/Swarm/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="java hdf5"

DEPEND="
	sys-devel/gcc[objc]
	dev-lang/tcl
	dev-lang/tk
	dev-tcltk/blt
	>=media-libs/libpng-1.2.5
	>=sys-libs/zlib-1.2.2
	x11-libs/libXpm
	virtual/emacs
	hdf5? ( >=sci-libs/hdf5-1.6.2 )
	java? ( >=virtual/jdk-1.4.2 )
"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/${P}-amd64.patch"
)

src_configure() {
	javaconf="--without-jdkdir"
	use java && javaconf="--with-jdkdir=`java-config -O`"
	hdf5conf="--without-hdf5dir"
	use hdf5 && hdf5conf="--with-hdf5dir"
	econf \
		--enable-shared \
		${javaconf} \
		${hdf5conf} \
		|| die "econf failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS NEWS README THANKS
}

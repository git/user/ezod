# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit distutils

DESCRIPTION="Python wrapper class for Tesseract"
HOMEPAGE="http://code.google.com/p/python-tesseract"
SRC_URI="http://${PN}.googlecode.com/files/${PN}_${PV}.orig.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	>=app-text/tesseract-3.00
"

# Copyright 2008-2012 Funtoo Technologies
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="cbmbasic - Commodore BASIC V2 as a scripting language"
HOMEPAGE="http://www.pagetable.com/?p=48"
SRC_URI="http://www.weihenstephan.org/~michaste/pagetable/recompiler/cbmbasic.zip"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND=""
RDEPEND=""

S=${WORKDIR}

src_install() {
	dobin cbmbasic
	dodoc README.txt
}
